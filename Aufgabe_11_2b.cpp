#include <iostream>
using namespace std;

int PosProdInt(unsigned int size, int const arr[]) {
    int posProd = 1;
    for (unsigned int i = 0; i < size; i++) {
        if (arr[i] > 0)
            posProd *= arr[i];
    }

    return posProd;
}

double PosProdDbl(unsigned int size, double const arr[]) {
    double posProd = 1;
    for (unsigned int i = 0; i < size; i++) {
        if (arr[i] > 0)
            posProd *= arr[i];
    }

    return posProd;
}

template<typename T>
T PosProd(unsigned int size, T const arr[]) {
    T posProd = 1;
    for (unsigned int i = 0; i < size; i++) {
        if (arr[i] > 0)
            posProd *= arr[i];
    }

    return posProd;
}

int main(void) {
    int const iA[] = {1, -1, 7, -6, -3, 0};
    double const dA[] = {1.3, -3.2, 0.1, -2.7, 0.4};

    cout << PosProdInt(6, iA) << " " << PosProdDbl(5, dA) << endl;
    cout << PosProd(6, iA) << " " << PosProd(5, dA) << endl;

    return 0;
}